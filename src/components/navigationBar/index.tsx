import React from 'react';
import { PageHeader, Tabs } from 'antd';
import { history, useLocation } from 'umi';
import './index.less';

const { TabPane } = Tabs;

const Navbar: React.FC = () => {
  const location = useLocation();

  const activeTabKey = location.pathname.split('/')[1];
  const handleChange = (key: string) => {
    history.push(`/${key}`);
  };

  return (
    <>
      <PageHeader
        className="site-page-header-responsive background-white headerArea"
        title="Welcome to VIZBACK"
        footer={
          <Tabs activeKey={activeTabKey} className="background-white" onChange={handleChange}>
            <TabPane tab="Overview" key="dashboard" />
            <TabPane tab="Feedback" key="feedback" />
            <TabPane tab="Insights" key="insights" />
            <TabPane tab="Notifications" key="notifications" />
            <TabPane tab="Integrations" key="integrations" />
            <TabPane tab="Settings" key="settings" />
          </Tabs>
        }
      />
    </>
  );
};
export default Navbar;

// .ant-pro-basicLayout-content
