import React from 'react';
import DetailCard from './detailCardComponent';
import CreateProjectCard from './createProjectCardComponent';
import './activeProjects.less';

const ActiveProjects: React.FC = () => {
  return (
    <div style={{ marginBottom: '20px' }}>
      <h3>Active(1)</h3>
      <div className="active-projects-container">
        <DetailCard />
        <CreateProjectCard />
      </div>
    </div>
  );
};

export default ActiveProjects;
