import React from 'react';
import { Input } from 'antd';
import './projectSearchBar.less';

const { Search } = Input;

const SearchBar: React.FC = () => {
  return (
    <>
      <Search
        placeholder="Search Projects"
        className="project-page-search-bar"
        style={{ width: '100%', marginBottom: '20px' }}
      />
    </>
  );
};

export default SearchBar;
