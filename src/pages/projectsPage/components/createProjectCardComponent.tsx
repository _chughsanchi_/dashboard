import React from 'react';
import './createProjectCard.less';
import { Link } from 'umi';

const CreateProjectCard: React.FC = () => {
  return (
    <div className="create-project-box">
      <Link to="/dashboard">Create Project</Link>
    </div>
  );
};

export default CreateProjectCard;
