import React from 'react';
import { Empty, Typography } from 'antd';

const { Title, Paragraph } = Typography;

const NoFeedback: React.FC = () => {
  return (
    <>
      <Empty
        style={{ backgroundColor: 'rgba(0,0,0,0.02)', margin: '20px auto', padding: '50px 50px' }}
        description={false}
      >
        <span>
          <Title level={4}>No Feedback found</Title>
          <Paragraph>
            Try adjusting your search or filter to find what you&#39;re looking for
          </Paragraph>
        </span>
      </Empty>
    </>
  );
};

export default NoFeedback;
