import React from 'react';
import { Row, Col } from 'antd';
import SideContent from './sideContent';
import FeedbackContent from './feedbackContent';

const MainContent: React.FC = () => {
  return (
    <div>
      <Row>
        <Col xs={24} sm={24} md={6} lg={6} xl={6}>
          <SideContent />
        </Col>
        <Col xs={24} sm={24} md={18} lg={18} xl={18}>
          <FeedbackContent />
        </Col>
      </Row>
    </div>
  );
};

export default MainContent;
