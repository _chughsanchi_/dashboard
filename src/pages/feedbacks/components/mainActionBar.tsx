import React from 'react';
import { Input, Select, Tooltip } from 'antd';
import './mainActionBar.less';

const { Search } = Input;
const { Option } = Select;

const ActionBar: React.FC = () => {
  return (
    <div className="action-bar-content">
      <Search placeholder="Search Feedback" style={{ width: 200, marginRight: '16px' }} />
      <Tooltip title="Group By">
        <Select defaultValue="year" style={{ width: 120 }}>
          <Option value="year">By year</Option>
          <Option value="month">By month</Option>
          <Option value="week">By week</Option>
          <Option value="day">By day</Option>
        </Select>
      </Tooltip>
    </div>
  );
};

export default ActionBar;
