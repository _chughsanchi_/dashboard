import React from 'react';
import { Row, Col, Badge, Progress, Divider, Typography } from 'antd';

import './feedbackComponent.less';

const { Title, Paragraph } = Typography;

interface Props {
  totalCount: number;
  openCount: number;
  inProgressCount: number;
  underReviewCount: number;
  resolvedCount: number;
}

const FeedbackComponent: React.FC<Props> = ({
  totalCount,
  openCount,
  inProgressCount,
  underReviewCount,
  resolvedCount,
}) => {
  return (
    <div className="feedback-container">
      <Title level={4}>Feedback</Title>
      <div className="box-style">
        <Row>
          <Col xs={24} sm={24} md={6} lg={4} xl={4}>
            <div className="leftBox">
              <h1>Total</h1>
              <Badge
                count={totalCount}
                style={{ backgroundColor: '#52c41a', width: '32px' }}
                showZero
              />
              <Paragraph>Date</Paragraph>
            </div>
            <Divider type="vertical" />
          </Col>
          <Col xs={24} sm={24} md={18} lg={20} xl={20}>
            <div className="rightBox">
              <Row>
                <Col span={24}>
                  <h2>Status</h2>
                </Col>
              </Row>
              <Divider />
              <Row>
                <Col span={12}>
                  <h1>Open</h1>
                </Col>
                <Col span={12}>
                  <div>
                    {openCount} <Progress percent={100} size="small" status="active" />
                  </div>
                </Col>
              </Row>
              <Divider />
              <Row>
                <Col span={12}>
                  <h1>In Progress</h1>
                </Col>
                <Col span={12}>
                  <Paragraph style={{ textAlign: 'left' }}>{inProgressCount}</Paragraph>
                </Col>
              </Row>
              <Divider />
              <Row>
                <Col span={12}>
                  <h1>Under Review</h1>
                </Col>
                <Col span={12}>
                  <Paragraph style={{ textAlign: 'left' }}>{underReviewCount}</Paragraph>
                </Col>
              </Row>
              <Divider />
              <Row>
                <Col span={12}>
                  <h1>Resolved</h1>
                </Col>
                <Col span={12}>
                  <Paragraph style={{ textAlign: 'left' }}>{resolvedCount}</Paragraph>
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default FeedbackComponent;
