import React from 'react';
import './index.less';
import SettingsComponent from './components/settingsComponent';

const Notification: React.FC = () => {
  return (
    <div className="notification-container">
      <div className="center">
        <SettingsComponent title="Feedback Created" description="feedback is created" />
        <SettingsComponent title="Feedback Assigned" description="feedback is assigned" />
        <SettingsComponent title="Feedback Deleted" description="feedback is deleted" />
        <SettingsComponent title="Comment Added" description="a comment is added" />
        <SettingsComponent title="Status Changed" description="feedback status is changed" />
      </div>
    </div>
  );
};

export default Notification;
