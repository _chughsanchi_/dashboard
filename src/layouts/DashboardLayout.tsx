import React from 'react';
import Navbar from '@/components/NavigationBar';

const DashboardLayout = ({ children }) => (
  <div>
    <Navbar />

    {children}
  </div>
);

export default DashboardLayout;
