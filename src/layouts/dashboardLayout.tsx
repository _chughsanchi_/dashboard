import React from 'react';
import Navbar from '@/components/navigationBar';
import { PageContainer } from '@ant-design/pro-layout';
import './dashboardLayout.less';

const Dashboardlayout: React.FC<{}> = ({ children }) => {
  return (
    <PageContainer pageHeaderRender={() => null}>
      <Navbar />
      {children}
    </PageContainer>
  );
};

export default Dashboardlayout;
